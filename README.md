We're thrilled to announce that "ECG Classification CNN" has officially moved from GitLab to GitHub! This decision was made to streamline our development process, leverage GitHub's robust features, and foster a more collaborative and engaging environment for our amazing community. We believe this move will enhance the overall experience for both contributors and users alike.

Thank you for your understanding and support during this transition. Together, let's make "ECG Classification CNN" on GitHub even more fantastic!

Happy Coding!

Best regards,
Armin Shoughi

P.S. Explore our new GitHub repository: https://github.com/arminshoughi/ecg-classification-cnn
